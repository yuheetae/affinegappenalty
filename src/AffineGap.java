
public class AffineGap {

    private static String seq1;
    private static String seq2;

    private int value;
    private AffineGap pointer;

    public AffineGap() {

    }

    public static void main(String[] args) {

        seq1 = args[0];
        seq2 = args[1];
        int n = args[0].length() + 1;
        int m = args[1].length() + 1;

        /*
        seq1 = "AAAGAATTCA";
        seq2 = "AAATCA";
        int n = seq1.length() + 1;
        int m = seq2.length() + 1;
        */

        int op = 6;
        int ext = 1;

        AffineGap[][] S = new AffineGap[m][n];
        AffineGap[][] I = new AffineGap[m][n];
        AffineGap[][] D = new AffineGap[m][n];

        for(int i=0; i<m; i++) {
            for(int j=0; j<n; j++) {
                S[i][j] = new AffineGap();
                I[i][j] = new AffineGap();
                D[i][j] = new AffineGap();
            }
        }

        S[0][0].value = 0;
        I[0][0].value = -1000;
        D[0][0].value = -1000;

        for(int i=1; i<m; i++) {
            S[i][0].value = -1000;
            I[i][0].value = -1000;

            if(i==1) {
                D[1][0].value = -op;
                D[1][0].pointer = S[0][0];
            }
            else {
                D[i][0].value = D[i-1][0].value - ext;
                D[i][0].pointer = D[i-1][0];
            }
        }



        for(int i=1; i<n; i++) {
            S[0][i].value = -1000;
            D[0][i].value = -1000;

            if(i==1) {
                I[0][1].value = -op;
                I[0][1].pointer = S[0][0];
            }
            else {
                I[0][i].value = I[0][i-1].value - ext;
                I[0][i].pointer = I[0][i-1];
            }
        }

        for(int i=1; i<m; i++) {
            for(int j=1; j<n; j++) {

                int score = getScore(i, j);

                //Calculate S(i, j)
                if(I[i-1][j-1].value > S[i-1][j-1].value && I[i-1][j-1].value > D[i-1][j-1].value) {
                    S[i][j].value = I[i-1][j-1].value + score;
                    S[i][j].pointer = I[i-1][j-1];
                }
                else if(D[i-1][j-1].value > S[i-1][j-1].value && D[i-1][j-1].value > I[i - 1][j - 1].value) {
                    S[i][j].value = D[i-1][j-1].value + score;
                    S[i][j].pointer = D[i-1][j-1];
                }
                else {
                    S[i][j].value = S[i - 1][j - 1].value + score;
                    S[i][j].pointer = S[i - 1][j - 1];
                }

                //Calculate I(i, j)
                if(S[i][j-1].value - op > I[i][j-1].value - ext) {
                    I[i][j].value = S[i][j-1].value - op;
                    I[i][j].pointer = S[i][j-1];
                }
                else {
                    I[i][j].value = I[i][j-1].value - ext;
                    I[i][j].pointer = I[i][j-1];
                }

                //Calculate D(i, j)

                if(S[i-1][j].value - op > D[i-1][j].value - ext) {
                    D[i][j].value = S[i-1][j].value - op;
                    D[i][j].pointer = S[i-1][j];
                }
                else {
                    D[i][j].value = D[i-1][j].value - ext;
                    D[i][j].pointer = D[i-1][j];
                }

                if(S[i][j].value == -1001) S[i][j].value = -1000;
                if(I[i][j].value == -1001) I[i][j].value = -1000;
                if(D[i][j].value == -1001) D[i][j].value = -1000;
            }
        }


        String resultS = printArray(S);
        System.out.println("S\n" + resultS);

        String resultI = printArray(I);
        System.out.println("I\n" + resultI);

        String resultD = printArray(D);
        System.out.println("D\n" + resultD);



        AffineGap current = null;
        int i=m-1;
        int j=n-1;
        String result1 = "";
        String result2 = "";
        if(S[m-1][n-1].value >= I[m-1][n-1].value) {
            if(S[m-1][n-1].value >= D[m-1][n-1].value) {
                current = S[m-1][n-1];
            }
        }

        else if(I[m-1][n-1].value >= D[m-1][n-1].value) {
            current = I[m-1][n-1];
        }

        else {
            current = D[m-1][n-1];
        }

        while(current.pointer != null) {
            if(current == S[i][j]) {
                result1 += seq1.charAt(j-1) + " ";
                result2 += seq2.charAt(i-1) + " ";
                current = S[i][j].pointer;
                i -= 1;
                j -= 1;
            }
            else if(current == I[i][j]) {
                result1 += seq1.charAt(j-1) + " ";
                result2 += "_ ";
                current = I[i][j].pointer;
                j -= 1;
            }
            else {
                result1 += "_ ";
                result2 += seq2.charAt(i-1) + " ";
                current = D[i][j].pointer;
                i -= 1;
            }
        }

        String r1 = new StringBuilder(result1).reverse().toString();
        String r2 = new StringBuilder(result2).reverse().toString();

        System.out.println(r1 + "\n" + r2);

    }

    private static String printArray(AffineGap[][] array) {
        int n = array[0].length;
        int m = array.length;

        String text = "";

        for(int i=0; i<m; i++) {
            for(int j=0; j<n; j++) {
                if(j==n-1) {
                    text += array[i][j].value + "\t}\n";
                }
                else if (j==0) {
                    text += "{\t" + array[i][j].value + "\t\t";
                }
                else {
                    text += array[i][j].value + ",\t\t";
                }
            }
        }

        return text;
    }


    private static int getScore(int i, int j) {

        char x = seq1.charAt(j-1);
        char y = seq2.charAt(i-1);

        if(x == y) return 5;

        return -2;
    }

}
