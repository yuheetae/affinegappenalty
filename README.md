# AffineGapPenalty #

### Description ###

* Java solution to pairwise alignment sequence problem with affine gap penalties
* Sequences are hardcoded so you need to edit the code to input your own sequences (this part is commented out in the code)